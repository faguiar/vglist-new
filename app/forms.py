from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, TextAreaField, SelectField, IntegerField
from wtforms.validators import DataRequired, Length
from flask_wtf.file import FileField, FileAllowed, FileRequired
from .models import User
from .defaults import *

class EditForm(Form):
	nickname = StringField('nickname', validators=[DataRequired()])
	about_me = TextAreaField('about_me', validators=[Length(min=0, max=140)])
	avatar = FileField('avatar', validators=[FileAllowed(['jpg', 'png', 'bmp','jpeg','gif'], 'Images only!')])
	banner = FileField('banner', validators=[FileAllowed(['jpg', 'png', 'bmp','jpeg','gif'], 'Images only!')])

	def __init__(self, original_nickname, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)
		self.original_nickname = original_nickname

	def validate(self):
		if not Form.validate(self):
			return False
		if self.nickname.data == self.original_nickname:
			return True
		user = User.query.filter_by(nickname=self.nickname.data).first()
		if user is not None:
			self.nickname.errors.append('This nickname is already in use. ''Please choose another one.')
			return False
		return True


class PostForm(Form):
    post = StringField('post', validators=[DataRequired()])


class SearchForm(Form):
    search = StringField('search', validators=[DataRequired()])
    type_of_search = SelectField(u'type', choices=[('games', 'Games'), ('posts', 'Broadcasts'), ('consoles', 'Platforms')])

class CreateGameForm(Form):
	name = StringField('name', validators=[DataRequired()])

	
class CreateGameVersionForm(Form):
	version_name = StringField('name', validators=[DataRequired()])
	genre = StringField('genre')
	boxart = FileField('image', validators=[FileAllowed(['jpg', 'png', 'bmp','jpeg','gif'],
											'Images only!')])
	description = TextAreaField('description')

class EditGameVersionForm(Form):
	version_name = StringField('name', validators=[DataRequired()])
	genre = StringField('genre')
	boxart = FileField('image', validators=[FileAllowed(['jpg', 'png', 'bmp','jpeg','gif'],
														'Images only!')])
	description = TextAreaField('description')


class CreatePlatformForm(Form):
	name = StringField('name', validators=[DataRequired()])
	description = TextAreaField('description')
	
class CreateCompanyForm(Form):
	name = StringField('name', validators=[DataRequired()])
	description = TextAreaField('description')
	
class EditPlatformForm(Form):
	name = StringField('name', validators=[DataRequired()])
	description = TextAreaField('description')
	image = FileField('image', validators=[FileAllowed(['jpg', 'png', 'bmp','jpeg','gif'],
											'Images only!')])
class EditCompanyForm(Form):
	name = StringField('name', validators=[DataRequired()])
	description = TextAreaField('description')
	
class AddListEntryForm(Form):
	score = IntegerField('score')
	
class EditListEntryForm(Form):
	score = IntegerField('score')
