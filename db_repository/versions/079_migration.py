from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
post = Table('post', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('body', VARCHAR(length=140)),
    Column('timestamp', DATETIME),
    Column('user_id', INTEGER),
    Column('type_of_act', VARCHAR(length=64)),
    Column('listentry_id', INTEGER),
)

list_entry = Table('list_entry', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('status', String(length=140)),
    Column('user', Integer),
    Column('game', Integer),
    Column('score', Integer),
    Column('timestamp', DateTime),
    Column('post', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['post'].columns['listentry_id'].drop()
    post_meta.tables['list_entry'].columns['post'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['post'].columns['listentry_id'].create()
    post_meta.tables['list_entry'].columns['post'].drop()
