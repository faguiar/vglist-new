genres = ['Action',
			'Shooter',
			'Adventure',
			'Role-Playing',
			'Simulation',
			'Strategy',
			'Sports',
			'Puzzle']

statuses = ['Playing',
			'Completed',
			'Played',
			'On-Hold',
			'Dropped',
			'Wished',
			]
