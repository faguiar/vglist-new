from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
game_son = Table('game_son', pre_meta,
    Column('son_id', INTEGER, primary_key=True, nullable=False),
    Column('version_name', VARCHAR(length=128)),
    Column('son_timestamp', DATETIME),
    Column('dad', INTEGER),
    Column('platform', INTEGER),
    Column('genre', VARCHAR(length=32)),
    Column('subgenre', VARCHAR(length=32)),
)

game_son = Table('game_son', post_meta,
    Column('son_id', Integer, primary_key=True, nullable=False),
    Column('version_name', String(length=128)),
    Column('son_timestamp', DateTime),
    Column('dad', Integer),
    Column('console', Integer),
    Column('genre', String(length=32)),
    Column('subgenre', String(length=32)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['game_son'].columns['platform'].drop()
    post_meta.tables['game_son'].columns['console'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['game_son'].columns['platform'].create()
    post_meta.tables['game_son'].columns['console'].drop()
