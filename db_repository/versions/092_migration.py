from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
gamesonghost = Table('gamesonghost', post_meta,
    Column('son_id', Integer, primary_key=True, nullable=False),
    Column('version_name', String(length=128)),
    Column('type_of_proposal', String(length=128)),
    Column('son_timestamp', DateTime),
    Column('description', String(length=300)),
    Column('dad', Integer),
    Column('console', Integer),
    Column('boxart', String(length=256)),
    Column('thumbnail', String(length=256)),
    Column('genre', String(length=32)),
    Column('subgenre', String(length=32)),
    Column('ghost_dad', Integer),
    Column('user_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['gamesonghost'].columns['type_of_proposal'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['gamesonghost'].columns['type_of_proposal'].drop()
