from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
company = Table('company', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=128)),
    Column('description', String(length=300)),
    Column('timestamp', DateTime),
)

game_son = Table('game_son', post_meta,
    Column('son_id', Integer, primary_key=True, nullable=False),
    Column('version_name', String(length=128)),
    Column('son_timestamp', DateTime),
    Column('dad', Integer),
    Column('console', Integer),
    Column('developers', Integer),
    Column('publishers', Integer),
    Column('boxart', String(length=256)),
    Column('thumbnail', String(length=256)),
    Column('genre', String(length=32)),
    Column('subgenre', String(length=32)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['company'].create()
    post_meta.tables['game_son'].columns['developers'].create()
    post_meta.tables['game_son'].columns['publishers'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['company'].drop()
    post_meta.tables['game_son'].columns['developers'].drop()
    post_meta.tables['game_son'].columns['publishers'].drop()
