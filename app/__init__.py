import os
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID
from flask.ext.mail import Mail
from config import basedir, ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, \
    MAIL_PASSWORD
from .momentjs import momentjs
from flask.ext.uploads import *
from flask.ext.admin import Admin
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.login import login_user, logout_user, current_user, \
    login_required

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
app.secret_key = 'abc123'

app.config['UPLOAD_FOLDER_BOXART'] = os.path.realpath('.') + '/media/boxart/'
app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL'] = os.path.realpath('.') + '/media/boxart/thumbs/'
app.config['UPLOAD_FOLDER_BOXART_GHOST'] = os.path.realpath('.') + '/media/ghosts/boxart'
app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL_GHOST'] = os.path.realpath('.') + '/media/ghosts/boxart/thumbs/'
app.config['UPLOAD_FOLDER_CONSOLE'] = os.path.realpath('.') + '/media/console/'
app.config['UPLOAD_FOLDER_CONSOLE_THUMBNAIL'] = os.path.realpath('.') + '/media/console/thumbs/'
app.config['UPLOAD_FOLDER_AVATAR'] = os.path.realpath('.') + '/media/avatar/'
app.config['UPLOAD_FOLDER_USERBANNER'] = os.path.realpath('.') + '/media/avatar/banner/'
app.config['UPLOAD_FOLDER_AVATAR_THUMBNAIL'] = os.path.realpath('.') + '/media/avatar/thumbs/'
app.config['DEFAULT_FILE_STORAGE'] = 'filesystem'

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
oid = OpenID(app, os.path.join(basedir, 'tmp'))
mail = Mail(app)


if not app.debug:
    import logging
    from logging.handlers import SMTPHandler
    credentials = None
    if MAIL_USERNAME or MAIL_PASSWORD:
        credentials = (MAIL_USERNAME, MAIL_PASSWORD)
    mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT),
                               'no-reply@' + MAIL_SERVER, ADMINS,
                               'microblog failure', credentials)
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

if not app.debug:
    import logging
    from logging.handlers import RotatingFileHandler
    file_handler = RotatingFileHandler('tmp/microblog.log', 'a',
                                       1 * 1024 * 1024, 10)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info('microblog startup')



# TEMPLATE FILTERS
app.jinja_env.globals['momentjs'] = momentjs

@app.template_filter('markdown')
def markdown_filter(data):
	 from flask import Markup 
	 from markdown import markdown 
	 return Markup(markdown(data))


# Admin Page
from app import views, models
from admin import *

admin = Admin(app)

admin.add_view(UserView(User, db.session))
admin.add_view(GenericProtectedView(ListEntry, db.session))
admin.add_view(GenericProtectedView(Post, db.session))
admin.add_view(GenericProtectedView(Game_dad, db.session))
admin.add_view(GenericProtectedView(Game_son, db.session))
admin.add_view(GenericProtectedView(Company, db.session))
db.create_all()


