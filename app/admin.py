from flask.ext.admin import Admin, BaseView
from flask.ext.admin.contrib.sqla import ModelView
from models import *
from flask.ext.login import login_user, logout_user, current_user, \
    login_required
from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, \
    login_required
from datetime import datetime
from app import app, db, lm, oid
from .forms import *
from .models import *
from .defaults import *
from .create_thumbnail import *
from .emails import follower_notification
from config import POSTS_PER_PAGE, MAX_SEARCH_RESULTS
from sqlalchemy import update
from flask.ext.uploads import *
from werkzeug import secure_filename
from flask import Flask,session, request, flash, url_for, redirect, render_template, abort ,g
from flask.ext.login import login_user , logout_user , current_user , login_required


class UserView(ModelView):	
	def is_accessible(self):
		if current_user.is_authenticated() and current_user.access_level:
			if current_user.access_level > 9:
				return True
		return False

	# Show only name and email columns in list view
	column_list = ('nickname', 'email')

	# Enable search functionality - it will search for terms in
	# name and email fields
	column_searchable_list = ('nickname', 'email')

	# Add filters for name and email columns
	column_filters = ('nickname', 'email')

class GenericProtectedView(ModelView):	
	def is_accessible(self):
		if current_user.is_authenticated() and current_user.access_level:
			if current_user.access_level > 9:
				return True
		return False


