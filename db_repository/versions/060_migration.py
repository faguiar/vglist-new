from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
manufacturer = Table('manufacturer', pre_meta,
    Column('platform_id', INTEGER, primary_key=True, nullable=False),
    Column('company_id', INTEGER, primary_key=True, nullable=False),
)

platform = Table('platform', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=128)),
    Column('description', String(length=300)),
    Column('timestamp', DateTime),
    Column('image', String(length=256)),
    Column('thumbnail', String(length=256)),
    Column('manufacturer', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['manufacturer'].drop()
    post_meta.tables['platform'].columns['manufacturer'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['manufacturer'].create()
    post_meta.tables['platform'].columns['manufacturer'].drop()
