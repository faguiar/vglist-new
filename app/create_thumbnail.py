import PIL
from PIL import Image

BASE_WIDTH = 180

def create_thumbnail(image):
	img = Image.open(image)
	wpercent = (BASE_WIDTH/float(img.size[0]))
	hsize = int((float(img.size[1])*float(wpercent)))
	img = img.resize((BASE_WIDTH,hsize), PIL.Image.ANTIALIAS)
	return img
