from hashlib import md5
from app import db
from app import app
from flask import send_from_directory
import sys, os
from werkzeug import generate_password_hash, check_password_hash
from flask.ext.admin import Admin
from flask.ext.admin.contrib.sqla import ModelView

if sys.version_info >= (3, 0):
    enable_search = False
else:
    enable_search = True
    import flask.ext.whooshalchemy as whooshalchemy


followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)

developers = db.Table(
    'developers',
    db.Column('game_id', db.Integer, db.ForeignKey('game_son.son_id'), nullable=False),
    db.Column('company_id', db.Integer, db.ForeignKey('company.id'), nullable=False),
    db.PrimaryKeyConstraint('game_id', 'company_id')
)

publishers = db.Table(
    'publishers',
    db.Column('game_id', db.Integer, db.ForeignKey('game_son.son_id'), nullable=False),
    db.Column('company_id', db.Integer, db.ForeignKey('company.id'), nullable=False),
    db.PrimaryKeyConstraint('game_id', 'company_id')
)
developers_ghost = db.Table(
    'developers_ghost',
    db.Column('game_id', db.Integer, db.ForeignKey('gamesonghost.son_id'), nullable=False),
    db.Column('company_id', db.Integer, db.ForeignKey('company.id'), nullable=False),
    db.PrimaryKeyConstraint('game_id', 'company_id')
)

publishers_ghost = db.Table(
    'publishers_ghost',
    db.Column('game_id_ghost', db.Integer, db.ForeignKey('gamesonghost.son_id'), nullable=False),
    db.Column('company_id_ghost', db.Integer, db.ForeignKey('company.id'), nullable=False),
    db.PrimaryKeyConstraint('game_id_ghost', 'company_id_ghost')
)

manufacturer = db.Table(
    'manufacturer',
    db.Column('platform_id', db.Integer, db.ForeignKey('platform.id'), nullable=False),
    db.Column('company_id', db.Integer, db.ForeignKey('company.id'), nullable=False),
    db.PrimaryKeyConstraint('platform_id', 'company_id')
)

class Game_dad(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=True)
    timestamp = db.Column(db.DateTime)
    score = db.Column(db.FLOAT(precision=3))
    num_favs = db.Column(db.Integer)
    versions = db.relationship('Game_son', backref='son', lazy='dynamic')
    
    def get_id():
	    return id
    
    def __repr__(self):
	    return '<Game %r> : %r' % (self.name, self.versions)

class Game_son(db.Model):
    __searchable__ = ['version_name']
    
    son_id = db.Column(db.Integer, primary_key=True)
    version_name = db.Column(db.String(128), index=True, unique=False)
    son_timestamp = db.Column(db.DateTime)
    description = db.Column(db.String(300))
    dad = db.Column(db.Integer, db.ForeignKey('game_dad.id'))
    console = db.Column(db.Integer, db.ForeignKey('platform.id'))
    developers = db.relationship('Company', secondary=developers, backref='devs')
    publishers = db.relationship('Company', secondary=publishers, backref='pubs')	
    boxart = db.Column(db.String(256))
    thumbnail = db.Column(db.String(256))
    genre = db.Column(db.String(32))
    subgenre = db.Column(db.String(32))
    users_list = db.relationship('ListEntry', backref='users_list')
    ghosts = db.relationship('Ghost_Game_son', backref='ghost', lazy='dynamic')

			    
    def __repr__(self):
	    return '<Game %r>: %r' % (self.version_name, self.dad)

class Ghost_Game_son(db.Model):  
    __tablename__ = 'gamesonghost'
    son_id = db.Column(db.Integer, primary_key=True)
    version_name = db.Column(db.String(128), index=True, unique=False)
    type_of_proposal = db.Column(db.String(128))
    son_timestamp = db.Column(db.DateTime)
    description = db.Column(db.String(300))
    dad = db.Column(db.Integer, db.ForeignKey('game_dad.id'))
    console = db.Column(db.Integer, db.ForeignKey('platform.id'))
    developers_ghost = db.relationship('Company', secondary=developers_ghost, backref='devs_ghost')	
    publishers_ghost = db.relationship('Company', secondary=publishers_ghost, backref='pubs_ghost')	
    boxart = db.Column(db.String(256))
    thumbnail = db.Column(db.String(256))
    genre = db.Column(db.String(32))
    subgenre = db.Column(db.String(32))
    ghost_dad = db.Column(db.Integer, db.ForeignKey('game_son.son_id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    
    def __repr__(self):
	    return '<Game %r>: %r' % (self.version_name, self.dad)


class Platform(db.Model):
    __searchable__ = ['name']
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=True)
    description = db.Column(db.String(300))
    timestamp = db.Column(db.DateTime)
    image = db.Column(db.String(256))
    thumbnail = db.Column(db.String(256))
    manufacturer = db.relationship('Company', secondary=manufacturer, backref='manufacturer')	
    games = db.relationship('Game_son', backref='platform', lazy='dynamic')
    
    def __repr__(self):
	    return '<Platform %r>: %r' % (self.id, self.name)
    

class Company(db.Model):
    __searchable__ = ['name']
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=True)
    description = db.Column(db.String(300))
    timestamp = db.Column(db.DateTime)

    def __repr__(self):
	    return "<Company %r>: %r" % (self.id,self.name)
	
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    ghosts = db.relationship('Ghost_Game_son', backref='ghost_author', lazy='dynamic')
    list_entries = db.relationship('ListEntry', backref='entries', lazy='dynamic')
    about_me = db.Column(db.String(140))
    pwdhash = db.Column(db.String(54))
    last_seen = db.Column(db.DateTime)
    timestamp = db.Column(db.DateTime)
    followed = db.relationship('User',
                               secondary=followers,
                               primaryjoin=(followers.c.follower_id == id),
                               secondaryjoin=(followers.c.followed_id == id),
                               backref=db.backref('followers', lazy='dynamic'),
                               lazy='dynamic')
			       
    avatar = db.Column(db.String(256))
    banner = db.Column(db.String(256))
    access_level = db.Column(db.Integer)
    
    def __init__(self, username, password, email, timestamp):
	self.nickname = username
	self.set_password(password)
	self.email = email
	self.timestamp = timestamp
	    
    def set_password(self, password):
	self.pwdhash = generate_password_hash(password)

    def check_password(self, password):
	return check_password_hash(self.pwdhash, password)
	
    @staticmethod
    def make_unique_nickname(nickname):
        if User.query.filter_by(nickname=nickname).first() is None:
            return nickname
        version = 2
        while True:
            new_nickname = nickname + str(version)
            if User.query.filter_by(nickname=new_nickname).first() is None:
                break
            version += 1
        return new_nickname

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def generate_avatar(self, size):
	return 'http://www.gravatar.com/avatar/%s?d=mm&s=%d' % \
	    (md5(self.email.encode('utf-8')).hexdigest(), size)	    
	
    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        return Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id).order_by(
                    Post.timestamp.desc())

    def __repr__(self):
        return '<User %r>' % (self.nickname)


class Post(db.Model):
    __searchable__ = ['body']

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    type_of_act = db.Column(db.String(64))
    entry = db.Column(db.Integer, db.ForeignKey('listentry.id'))

    def __repr__(self):
        return '<Post %r>' % (self.body)

class ListEntry(db.Model):
    __tablename__ = 'listentry'
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(140), index=True)
    user = db.Column(db.Integer, db.ForeignKey('user.id'))
    game = db.Column(db.Integer, db.ForeignKey('game_son.son_id'))
    score = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime)
    feed = db.relationship('Post', backref='broadcast_feed', lazy='dynamic')	

    def __repr__(self):
	return '<User: %r> <Game: %r> <Status: %r>' % (self.user, self.game, self.status)
    
if enable_search:
    whooshalchemy.whoosh_index(app, Post)
    whooshalchemy.whoosh_index(app, Game_son)
    whooshalchemy.whoosh_index(app, Platform)
