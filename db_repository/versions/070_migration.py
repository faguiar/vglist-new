from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
post = Table('post', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('body', String(length=140)),
    Column('timestamp', DateTime),
    Column('user_id', Integer),
    Column('type_of_act', String(length=64)),
    Column('list_entry', Integer),
)

list_entry = Table('list_entry', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('status', VARCHAR(length=140)),
    Column('user', INTEGER),
    Column('game', INTEGER),
    Column('score', INTEGER),
    Column('timestamp', DATETIME),
    Column('post', INTEGER),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['post'].columns['list_entry'].create()
    pre_meta.tables['list_entry'].columns['post'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['post'].columns['list_entry'].drop()
    pre_meta.tables['list_entry'].columns['post'].create()
