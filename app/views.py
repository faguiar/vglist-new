from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, \
    login_required
from datetime import datetime
from app import app, db, lm, oid
from .forms import *
from .models import *
from .defaults import *
from .create_thumbnail import *
from .emails import follower_notification
from config import POSTS_PER_PAGE, MAX_SEARCH_RESULTS
from sqlalchemy import update
from flask.ext.uploads import *
from werkzeug import secure_filename
from flask import Flask,session, request, flash, url_for, redirect, render_template, abort ,g
from flask.ext.login import login_user , logout_user , current_user , login_required


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()
        g.search_form = SearchForm()

@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@app.route('/index/<int:page>', methods=['GET', 'POST'])
@login_required
def index(page=1):
    form = PostForm()
    if form.validate_on_submit():
        post = Post(body=form.post.data, timestamp=datetime.utcnow(),
                    author=g.user, type_of_act="broadcast")
        db.session.add(post)
        db.session.commit()
        flash('Your post is now live!')
        return redirect(url_for('index'))
    posts = g.user.followed_posts().paginate(page, POSTS_PER_PAGE, False)
    return render_template('index.html',
                           title='Home',
                           form=form,
                           posts=posts)

@app.route('/register/' , methods=['GET','POST'])
def register():
	if request.method == 'GET':
		return render_template('register.html')
	else:
		nickname = request.form['username']
		password = request.form['password']
		email = request.form['email']
		usercheck = User.query.filter_by(nickname=nickname).first()
		emailcheck = User.query.filter_by(email=email).first()
		if(usercheck or emailcheck):
			flash("Email or nickname already on our database.", 'error')
			return redirect(url_for('register'))
		else:
			user = User(nickname, password, email, datetime.utcnow())
			user.access_level=1
			db.session.add(user)
			db.session.commit()
			db.session.add(user.follow(user))
			db.session.commit()
			flash('User successfully registered')
			return redirect(url_for('login'))
 
@app.route('/login/',methods=['GET','POST'])
def login():
	if request.method == 'GET':
		return render_template('login.html')
		
	nickname = request.form['username']
	password = request.form['password']
	registered_user = User.query.filter_by(nickname=nickname).first()
	if registered_user is None:
		flash('Username is invalid' , 'error')
		return redirect(url_for('login'))
	
	if registered_user.check_password(password):
		remember_me = False
		if 'remember_me' in request.form:
			remember_me = True

		login_user(registered_user, remember = remember_me)
		flash('Logged in successfully')
		return redirect(request.args.get('next') or url_for('index'))
	
	else:
		flash('Invalid password, fella...', 'error')
		return redirect(url_for('login'))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/user/<nickname>')
@app.route('/user/<nickname>/<int:page>')
@login_required
def user(nickname, page=1):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return redirect(url_for('index'))
    posts = user.posts.paginate(page, POSTS_PER_PAGE, False)
    return render_template('user.html',
                           user=user,
                           posts=posts)

@app.route('/user/<nickname>/list/')
@app.route('/user/<nickname>/list/<int:page>')
def user_list(nickname, page=1):
	entries = ListEntry.query.filter_by(user=nickname).all()
	user = User.query.filter_by(nickname=nickname).first()
	edit_list = EditListEntryForm()
	return render_template('user_list.html', entries=entries, user=user, edit_form=edit_list, statuses=statuses)

@app.route('/edit/', methods=['GET', 'POST'])
@login_required
def edit():
	form = EditForm(g.user.nickname)
	if form.validate_on_submit():
		g.user.nickname = form.nickname.data
		g.user.about_me = form.about_me.data
		avatar = request.files['avatar']	
		if avatar:
			filename = secure_filename(avatar.filename)
			avatar_path = os.path.join(app.config['UPLOAD_FOLDER_AVATAR'], filename)
			avatar.save(avatar_path)
			avatar_thumbnail = create_thumbnail(avatar)
			avatar_thumbnail_path = os.path.join(app.config['UPLOAD_FOLDER_AVATAR_THUMBNAIL'],filename)
			avatar_thumbnail.save(avatar_thumbnail_path)
		else:
			boxart = None
			boxart_thumbnail = None	
			filename = None
			
		banner = request.files['banner']	
		if banner:
			banner_filename = secure_filename(banner.filename)
			banner_path = os.path.join(app.config['UPLOAD_FOLDER_USERBANNER'], banner_filename)
			banner.save(banner_path)
		else:
			banner = None
			banner_filename = None
		
		if filename:
			g.user.avatar = filename
		if banner_filename:
			g.user.banner = banner_filename
		
		db.session.add(g.user)
		db.session.commit()
		flash('Your changes have been saved.')
		return redirect(url_for('edit'))
        
	elif request.method != "POST":
		form.nickname.data = g.user.nickname
		form.about_me.data = g.user.about_me
	return render_template('edit.html', form=form)

@app.route('/<nickname>/followers/', methods=['GET','POST'])
@login_required
def followers_list(nickname):
	user = User.query.filter_by(nickname=nickname).first()
	followers = user.followers.all()
	for i in followers:
		if i.nickname == nickname:
			followers.remove(i)
			
	return render_template('followers_list.html', followers=followers, user=user)

@app.route('/<nickname>/followed/', methods=['GET','POST'])
@login_required
def followed_list(nickname):
	user = User.query.filter_by(nickname=nickname).first()
	followed = user.followed.all()
	for i in followed:
		if i.nickname == nickname:
			followed.remove(i)
			
	return render_template('followed_list.html', followed=followed, user=user)

@app.route('/follow/<nickname>')
@login_required
def follow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return redirect(url_for('index'))
    if user == g.user:
        flash('You can\'t follow yourself!')
        return redirect(url_for('user', nickname=nickname))
    u = g.user.follow(user)
    if u is None:
        flash('Cannot follow ' + nickname + '.')
        return redirect(url_for('user', nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash('You are now following ' + nickname + '!')
    follower_notification(user, g.user)
    return redirect(url_for('user', nickname=nickname))

@app.route('/unfollow/<nickname>')
@login_required
def unfollow(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return redirect(url_for('index'))
    if user == g.user:
        flash('You can\'t unfollow yourself!')
        return redirect(url_for('user', nickname=nickname))
    u = g.user.unfollow(user)
    if u is None:
        flash('Cannot unfollow ' + nickname + '.')
        return redirect(url_for('user', nickname=nickname))
    db.session.add(u)
    db.session.commit()
    flash('You have stopped following ' + nickname + '.')
    return redirect(url_for('user', nickname=nickname))

@app.route('/search/', methods=['POST'])
@login_required
def search():
	if not g.search_form.validate_on_submit():
		return redirect(url_for('index'))     
	return redirect(url_for('search_results', query = g.search_form.search.data, 
							type_of_search = g.search_form.type_of_search.data))

@app.route('/search_results/<type_of_search>/<query>/')
@login_required
def search_results(query, type_of_search):
	if(type_of_search == 'games'):
		results = Game_son.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
	elif(type_of_search == 'posts'):
		results = Post.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
	elif(type_of_search == 'consoles'):
		results = Platform.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
	return render_template('search_results.html',
							query = query,
							results = results,
							type_of_search = type_of_search)

@app.route('/game/<dad_id>/<son_id>/add_list_entry/', methods=['POST'])
@login_required
def add_list_entry(dad_id, son_id):
	form = AddListEntryForm()
	game = Game_son.query.filter_by(dad = dad_id, son_id = son_id).first()
	if game:
		user = g.user
		if user:
			status = request.form['status']
			if status:
				if form.score.data:
					score = form.score.data
				else:
					score = None
					
				list_entry = ListEntry(status=status, user=user.nickname, game=game.son_id, score=score,
							timestamp=datetime.utcnow())
				try:
					db.session.add(list_entry)
					db.session.commit()
					flash("Game "+game.version_name+" added to your list!" )
				except:
					flash("OOps, something went wrong while creating your list entry...")
				
				if score:
					game_score_refresh(game)

				post = Post(body="added", timestamp=datetime.utcnow(), author=g.user, type_of_act="list_entry", entry=list_entry.id)
				try:
					db.session.add(post)
					db.session.commit()
				except:
					flash("Oops, something went wrong sending the feed to our broadcasts... We're sorry.")
					
				return redirect(url_for('game', dad_id=dad_id, son_id=son_id))
	flash("Oops, something went wrong... We're sorry.")

@app.route('/game/<dad_id>/<son_id>/edit_list_entry/', methods=['GET', 'POST'])
@login_required
def edit_list_entry(dad_id, son_id):
	edit_form = EditListEntryForm()
	game = Game_son.query.filter_by(dad = dad_id, son_id = son_id).first()
	entry = ListEntry.query.filter_by(user=g.user.nickname, game=game.son_id).first()

	if game and entry and g.user:
		status = request.form['status']
		if status:
			if edit_form.score.data:
				score = edit_form.score.data
			else:
				score = None
				
			try:
				entry.status = status
				entry.score = score
				db.session.add(entry)
				db.session.commit()
				flash("List entry for: "+game.version_name +' - '+ game.platform.name +" successfully edited!" )

			except:
				flash("Oops, something went wrong... We're sorry.")
				
			if entry.score:
				game_score_refresh(game)
			
			post = Post.query.filter_by(broadcast_feed=entry).all()
			if post:
				for i in post:
					db.session.delete(i)
				db.session.commit()
				
			post = Post(body="edited", timestamp=datetime.utcnow(), author=g.user, type_of_act="list_entry", entry=entry.id)
			try:
				db.session.add(post)
				db.session.commit()
			except:
				flash("Oops, something went wrong sending the feed to our broadcasts... We're sorry.")

			return redirect(url_for('game', dad_id=dad_id, son_id=son_id))
	flash("Oops, something went wrong... We're sorry. Cannot get status.")

@app.route('/game/<list_id>/<son_id>/delete_list_entry/', methods=['GET', 'POST'])
@login_required
def delete_list_entry(list_id, son_id):
	entry = ListEntry.query.filter_by(user=g.user.nickname, game=son_id).first()
	entries = ListEntry.query.filter_by(user=g.user.nickname).all()
	if entry:
		post = Post.query.filter_by(broadcast_feed=entry).all()
		if post:
			for i in post:
				db.session.delete(i)
		db.session.delete(entry)
		db.session.commit()
		flash("List entry deleted!")
		return render_template('user_list.html', entries=entries, user=g.user)
	else:
		return not_found_error(404)

def game_score_refresh(game):
	game_dad = Game_dad.query.filter_by(id=game.dad).first()
	entries = ListEntry.query.filter_by(game=game.son_id).all()
	total = 0
	num_entries = 0
	if entries:
		for entry in entries:
			if entry.score:
				total += entry.score
				num_entries += 1
		score = int(total/num_entries)
			
	game_dad.score = score
	db.session.add(game_dad)
	db.session.commit()
	return True

@app.route('/game/<dad_id>/<son_id>/', methods=['GET', 'POST'])
def game(dad_id, son_id):
	form = AddListEntryForm()
	edit_form = EditListEntryForm()
	game_dad = Game_dad.query.filter_by(id=dad_id).first()
	game_son = Game_son.query.filter_by(son_id=son_id).first()
	onlist = False
	list_entries = ListEntry.query.filter_by(user=g.user.nickname, game=game_son.son_id).first()
	
	if list_entries:
		edit_form.score.data = list_entries.score
		onlist = True
	
	return render_template('game.html', game_dad=game_dad, game_son=game_son, statuses=statuses,
							form=form, onlist=onlist, entry=list_entries, edit_form=edit_form)

@app.route('/game/', methods=['GET', 'POST'])
@app.route('/games/', methods=['GET', 'POST'])
def game_all():
	games = Game_dad.query.all()
	if(games):
		return render_template('show_games.html', games = games)
	else:
		return not_found_error(404)

@app.route('/game/add/', methods=['GET', 'POST'])
@login_required
def add_game():
	form = CreateGameForm()
	error = None
	if form.validate_on_submit():
		try:
			game = Game_dad(name=form.name.data, timestamp=datetime.utcnow())
			db.session.add(game)
			db.session.commit()
			#automatically create a son version of a game_dad
			game_son = Game_son(version_name= form.name.data, dad=game.id, son_timestamp=datetime.utcnow())
			db.session.add(game_son)
			db.session.commit()
		except:
			not_found_error(404)
		flash('Thank you for helping us!')
		return redirect(url_for('edit_game_version', dad_id = game.id, son_id = game_son.son_id))
	return render_template('create_game.html', error = error, form = form)

@app.route('/game/<dad_id>/add/', methods=['GET', 'POST'])
@login_required
def add_game_version(dad_id):
	form = CreateGameVersionForm()
	error = None
	game_dad = Game_dad.query.filter_by(id=dad_id).first()
	platforms = Platform.query.all()
	companies = Company.query.all()
	if game_dad:
		if form.validate_on_submit():
			if g.user.access_level and g.user.access_level > 5:
				if request.form['genre']:
					genre = request.form['genre']
				else:
					genre = None
					
				if request.form['platform']:
					platform = request.form['platform']
				else:
					platform = None
				
				tempdevs = request.form.getlist('developers')
				if tempdevs:
					tempcomp = []
					for i in tempdevs:
						tempcomp.append(Company.query.filter_by(id=i).first())
				else:
					tempcomp = []
					
				temppubs = request.form.getlist('publishers')
				if temppubs:
					temppubscomp = []
					for i in temppubs:
						temppubscomp.append(Company.query.filter_by(id=i).first())
				else:
					temppubscomp = []
					
				boxart = request.files['boxart']	
				if boxart:
					filename = secure_filename(boxart.filename)
					boxart_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART'], filename)
					boxart.save(boxart_path)
					boxart_thumbnail = create_thumbnail(boxart)
					boxart_thumbnail_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL'],filename)
					boxart_thumbnail.save(boxart_thumbnail_path)
				else:
					boxart = None
					boxart_thumbnail = None	
					filename = None
							
				game = Game_son(version_name=form.version_name.data, dad=dad_id, son_timestamp=datetime.utcnow(), 
								genre=genre, console=platform, boxart=filename, thumbnail = filename, developers = tempcomp,
								description = form.description.data, publishers=temppubscomp)
								
				db.session.add(game)
				db.session.commit()
				
				if Ghost_Game_son.query.filter_by(dad=game.dad, version_name=game.version_name).first():
					a = Ghost_Game_son.query.filter_by(dad=game.dad, version_name=game.version_name).first()
					delete_game_ghost(a.son_id)
					
				flash("Thank your for helping us!")
				return redirect(url_for('game', dad_id = dad_id, son_id = game.son_id))
				
			else:
				if request.form['genre']:
					genre = request.form['genre']
				else:
					genre = None
					
				if request.form['platform']:
					platform = request.form['platform']
				else:
					platform = None
				
				tempdevs = request.form.getlist('developers')
				if tempdevs:
					tempcomp = []
					for i in tempdevs:
						tempcomp.append(Company.query.filter_by(id=i).first())
				else:
					tempcomp = []
					
				temppubs = request.form.getlist('publishers')
				if temppubs:
					temppubscomp = []
					for i in temppubs:
						temppubscomp.append(Company.query.filter_by(id=i).first())
				else:
					temppubscomp = []
					
				boxart = request.files['boxart']
				if boxart:
					filename = secure_filename(boxart.filename)
					boxart_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART_GHOST'], filename)
					boxart.save(boxart_path)
					boxart_thumbnail = create_thumbnail(boxart)
					boxart_thumbnail_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL_GHOST'],filename)
					boxart_thumbnail.save(boxart_thumbnail_path)
				else:
					boxart = None
					boxart_thumbnail = None	
					filename = None
							
				game = Ghost_Game_son(version_name=form.version_name.data, dad=dad_id, son_timestamp=datetime.utcnow(), 
								genre=genre, console=platform, boxart=filename, thumbnail = filename, developers_ghost = tempcomp,
								description = form.description.data, publishers_ghost=temppubscomp, type_of_proposal='add', user_id=g.user.id)
								
				db.session.add(game)
				db.session.commit()
				flash("Thank your for helping us! Your contribution will be reviewd by one of our staff members! Check again in a \
				couple of hours. ")
				return redirect(url_for('game', dad_id = dad_id, son_id = game.son_id))
				


		return render_template('create_game_version.html', error=error, form=form, genres=genres,
								game=game_dad, platforms=platforms, companies=companies)

	else:
		return not_found_error(404)

@app.route('/game/<dad_id>/<son_id>/edit/', methods=['GET','POST'])
@login_required
def edit_game_version(dad_id, son_id):
	form = EditGameVersionForm()
	edit_form = EditGameVersionForm()
	error = None
	game_son = Game_son.query.filter_by(son_id=son_id).first()
	platforms = Platform.query.all()
	companies = Company.query.all()
	developers = game_son.developers
	if game_son:
		if form.validate_on_submit():
			if g.user.access_level and g.user.access_level > 5:

				if form.version_name.data:
					if(game_son.version_name != form.version_name.data):
						game_son.version_name = form.version_name.data
				else:
					error = "Name is a required field."
					
				if form.genre.data:
					if(game_son.genre != request.form['genre']):
						game_son.genre = request.form['genre']
						
				if request.form['platform']:
					this_platform = Platform.query.filter_by(id=request.form['platform']).first()
					game_son.platform = this_platform
				else:
					platform = None			

				tempdevs = request.form.getlist('developers')
				if tempdevs:
					tempcomp = []
					for i in tempdevs:
						tempcomp.append(Company.query.filter_by(id=i).first())
					game_son.developers = tempcomp
				
				temppubs = request.form.getlist('publishers')
				if temppubs:
					temppubscomp = []
					for i in temppubs:
						temppubscomp.append(Company.query.filter_by(id=i).first())
					game_son.publishers = temppubscomp
					
				boxart = request.files['boxart']
				if boxart:
					filename = secure_filename(boxart.filename)
					boxart_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART'], filename)
					boxart.save(boxart_path)
					game_son.boxart = filename
					boxart_thumbnail = create_thumbnail(boxart)
					boxart_thumbnail_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL'],filename)
					boxart_thumbnail.save(boxart_thumbnail_path)
					game_son.thumbnail = filename
					
				if form.description.data:
					game_son.description = form.description.data
					
				db.session.add(game_son)
				db.session.commit()
				flash("Game edited...")
				return redirect(url_for('game', dad_id = dad_id, son_id = son_id))
			
			else:
				if form.version_name.data:
					version_name = game_son.version_name
				else:
					error = "Name is a required field."
					
				if form.genre.data:
					if(game_son.genre != request.form['genre']):
						genre = request.form['genre']
						
				if request.form['platform']:
					this_platform = Platform.query.filter_by(id=request.form['platform']).first()
				else:
					this_platform = game_son.console			

				tempdevs = request.form.getlist('developers')
				if tempdevs:
					tempcomp = []
					for i in tempdevs:
						tempcomp.append(Company.query.filter_by(id=i).first())
				else:
					tempcomp = []
					
				temppubs = request.form.getlist('publishers')
				if temppubs:
					temppubscomp = []
					for i in temppubs:
						temppubscomp.append(Company.query.filter_by(id=i).first())
				else:
					temppubscomp = []
					
				boxart = request.files['boxart']
				if boxart:
					filename = secure_filename(boxart.filename)
					boxart_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART_GHOST'], filename)
					boxart.save(boxart_path)
					boxart_thumbnail = create_thumbnail(boxart)
					boxart_thumbnail_path = os.path.join(app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL_GHOST'],filename)
					boxart_thumbnail.save(boxart_thumbnail_path)
				else:
					filename = None
					
				if form.description.data:
					description = form.description.data
				else:
					description = game_son.description
					
				game = Ghost_Game_son(version_name=form.version_name.data, dad=dad_id, son_timestamp=datetime.utcnow(), 
								genre=genre, console=this_platform, boxart=filename, thumbnail = filename, developers_ghost = tempcomp,
								description = description, publishers_ghost=temppubscomp, type_of_proposal='edit', user_id=g.user.id)
				if game:
					db.session.add(game)
					db.session.commit()
				else:
					flash("Failed to create your edit proposal")
				flash("Your edit proposal will be reviewed by our staff...")
				return redirect(url_for('game', dad_id = dad_id, son_id = son_id))
				
		return render_template('edit_game_version.html', error=error, form=form, game=game_son, genres=genres,
								platforms=platforms, companies=companies)
	else:
		return not_found_error(404)

@app.route('/platform/add/', methods=['GET','POST'])
@login_required
def add_platform():
	form = CreatePlatformForm()
	error = None
	if form.validate_on_submit():
		try:
			platform = Platform(name=form.name.data, timestamp=datetime.utcnow(), description = form.description.data)
			db.session.add(platform)
			db.session.commit()
		except:
			not_found_error(404)
		flash('Thank you for helping us!')
		return redirect(url_for('platform', id = platform.id))
	return render_template('create_platform.html', error = error, form = form)

@app.route('/platform/<id>/edit/', methods=['GET','POST'])
@login_required
def edit_platform(id):
	form = EditPlatformForm()
	error = None
	platform = Platform.query.filter_by(id=id).first()
	companies = Company.query.all()
	if form.validate_on_submit():
		tempmanuf = request.form.getlist('manufacturer')
		if tempmanuf:
			tempmanufcomp = []
			for i in tempmanuf:
				tempmanufcomp.append(Company.query.filter_by(id=i).first())
			platform.manufacturer = tempmanufcomp
					
		image = request.files['image']
		if image:
			filename = secure_filename(image.filename)
			image_path = os.path.join(app.config['UPLOAD_FOLDER_CONSOLE'], filename)
			image.save(image_path)
			platform.image = filename
			image_thumbnail = create_thumbnail(image)
			image_thumbnail_path = os.path.join(app.config['UPLOAD_FOLDER_CONSOLE_THUMBNAIL'],filename)
			image_thumbnail.save(image_thumbnail_path)
			platform.thumbnail = filename

		platform.name = form.name.data
		platform.description = form.description.data
		db.session.add(platform)
		db.session.commit()
		return redirect(url_for('platform', id = id))

	elif request.method != "POST":
		form.name.data = platform.name
		form.description.data = platform.description
	return render_template('edit_platform.html', error = error, form = form, companies=companies, platform=platform)
			
@app.route('/platform/<id>/')
def platform(id):
	platform = Platform.query.filter_by(id = id).first()
	if platform:
		return render_template('platform.html', platform = platform)
	return not_found_error(404)

@app.route('/company/<id>/edit/', methods=['GET','POST'])
@login_required
def edit_company(id):
	form = EditCompanyForm()
	error = None
	company = Company.query.filter_by(id=id).first()
	if form.validate_on_submit():
		try:
			company.name = form.name.data
			company.description = form.description.data
			db.session.add(company)
			db.session.commit()
			return redirect(url_for('company', id = id))
		except:
			return not_found_error(404)

	elif request.method != "POST":
		form.name.data = company.name
		form.description.data = company.description
	return render_template('edit_company.html', error = error, form = form)

@app.route('/company/add/', methods=['GET','POST'])
@login_required
def add_company():
	form = CreateCompanyForm()
	error = None
	if form.validate_on_submit():
		try:
			company = Company(name=form.name.data, timestamp=datetime.utcnow(), description = form.description.data)
			db.session.add(company)
			db.session.commit()
		except:
			not_found_error(404)
		flash('Thank you for helping us!')
		return redirect(url_for('company', id = company.id))
	return render_template('create_company.html', error = error, form = form)

@app.route('/company/<id>/')	
def company(id):
	company = Company.query.filter_by(id = id).first()
	if company:
		return render_template('company.html', company = company)
	return not_found_error(404)


#GHOST'S MANAGERS
@app.route('/admin/games/ghosts')
def show_game_ghosts():
	ghosts = Ghost_Game_son.query.all()
	if g.user.access_level and g.user.access_level > 5:
		return render_template('admin/show_game_ghosts.html', ghosts = ghosts)
	else:
		return not_found_error(404)

@app.route('/admin/games/ghosts/<id>/', methods=['GET','POST'])
def review_game_ghost(id):
	form = CreateGameVersionForm()
	edit_form = EditGameVersionForm()
	ghost = Ghost_Game_son.query.filter_by(son_id=id).first()
	companies = Company.query.all()
	platforms = Platform.query.all()
	if g.user.access_level and g.user.access_level > 5:
		return render_template('admin/review_game_ghost.html', game = ghost, form = form, companies=companies,
								genres=genres, platforms=platforms, edit_form=edit_form)
	else:
		return not_found_error(404)
		
@app.route('/admin/games/ghosts/<id>/delete/')
def delete_game_ghost(id):
	ghost = Ghost_Game_son.query.filter_by(son_id = id).first()
	if g.user.access_level and g.user.access_level > 5:
		db.session.delete(ghost)
		db.session.commit()
		return redirect(url_for('show_game_ghosts'))
	else:
		not_found_error(404)

#MEDIA QUERIES
@app.route('/boxart/<filename>')
def uploaded_boxart(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_BOXART'],filename)

@app.route('/boxart/thumbnail/<filename>')
def uploaded_boxart_thumbnail(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL'],filename)
	
@app.route('/boxart-ghost/<filename>')
def uploaded_boxart_ghost(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_BOXART_GHOST'],filename)
	
@app.route('/boxart-ghost/thumbnail/<filename>')
def uploaded_boxart_thumbnail_ghost(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_BOXART_THUMBNAIL_GHOST'],filename)
	
@app.route('/platform/<filename>')
def uploaded_platform(filename):
	return send_from_directory(app.config['UPLOAD_CONSOLE_BOXART'],filename)
	
@app.route('/platform/thumbnail/<filename>')
def uploaded_platform_thumbnail(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_CONSOLE_THUMBNAIL'],filename)

@app.route('/avatar/<filename>')
def uploaded_avatar(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_AVATAR'],filename)
	
@app.route('/user/banner/<filename>')
def uploaded_user_banner(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER_USERBANNER'],filename)
