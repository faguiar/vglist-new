from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
game_son = Table('game_son', pre_meta,
    Column('son_id', INTEGER, primary_key=True, nullable=False),
    Column('version_name', VARCHAR(length=128)),
    Column('son_timestamp', DATETIME),
    Column('dad', INTEGER),
    Column('genre', VARCHAR(length=32)),
    Column('subgenre', VARCHAR(length=32)),
    Column('console', INTEGER),
    Column('boxart', VARCHAR(length=256)),
    Column('thumbnail', VARCHAR(length=256)),
    Column('developers', INTEGER),
    Column('publishers', INTEGER),
)

game_son = Table('game_son', post_meta,
    Column('son_id', Integer, primary_key=True, nullable=False),
    Column('version_name', String(length=128)),
    Column('son_timestamp', DateTime),
    Column('dad', Integer),
    Column('console', Integer),
    Column('devs', Integer),
    Column('pubs', Integer),
    Column('boxart', String(length=256)),
    Column('thumbnail', String(length=256)),
    Column('genre', String(length=32)),
    Column('subgenre', String(length=32)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['game_son'].columns['developers'].drop()
    pre_meta.tables['game_son'].columns['publishers'].drop()
    post_meta.tables['game_son'].columns['devs'].create()
    post_meta.tables['game_son'].columns['pubs'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['game_son'].columns['developers'].create()
    pre_meta.tables['game_son'].columns['publishers'].create()
    post_meta.tables['game_son'].columns['devs'].drop()
    post_meta.tables['game_son'].columns['pubs'].drop()
