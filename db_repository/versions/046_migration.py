from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
game_son = Table('game_son', pre_meta,
    Column('son_id', INTEGER, primary_key=True, nullable=False),
    Column('version_name', VARCHAR(length=128)),
    Column('son_timestamp', DATETIME),
    Column('dad', INTEGER),
    Column('genre', VARCHAR(length=32)),
    Column('subgenre', VARCHAR(length=32)),
    Column('console', INTEGER),
    Column('boxart', VARCHAR(length=256)),
    Column('thumbnail', VARCHAR(length=256)),
    Column('devs', INTEGER),
    Column('pubs', INTEGER),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['game_son'].columns['devs'].drop()
    pre_meta.tables['game_son'].columns['pubs'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['game_son'].columns['devs'].create()
    pre_meta.tables['game_son'].columns['pubs'].create()
