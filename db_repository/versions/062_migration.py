from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
manufacturer = Table('manufacturer', post_meta,
    Column('platform_id', Integer, primary_key=True, nullable=False),
    Column('company_id', Integer, primary_key=True, nullable=False),
)

platform = Table('platform', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('name', VARCHAR(length=128)),
    Column('description', VARCHAR(length=300)),
    Column('timestamp', DATETIME),
    Column('image', VARCHAR(length=256)),
    Column('thumbnail', VARCHAR(length=256)),
    Column('manufacturer', INTEGER),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['manufacturer'].create()
    pre_meta.tables['platform'].columns['manufacturer'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['manufacturer'].drop()
    pre_meta.tables['platform'].columns['manufacturer'].create()
